<?php
class Common_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function insert($table,$data)
    {
		$this->db->protect_identifiers=true;
        $this->db->insert($table,$data);
       // echo $this->db->last_query();die;
        return $this->db->insert_id();
        
    }
    
    function insert_batch($table,$data)
    {	
		$this->db->protect_identifiers=true;
        $this->db->insert_batch($table,$data);
		//return $this->db->insert_id();
    }
    
    function update($table,$where=array(),$data)
    {
        $this->db->update($table,$data,$where);
        //echo $this->db->last_query();die;
        return $this->db->affected_rows();
    }
    
    function updateIncrement($table,$where=array(),$data)
    {
        foreach ($data AS $k => $v)
		{
			$this->db->set($k, $v, FALSE);
		}
        foreach ($where AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->update($table);
		return $this->db->affected_rows();
    }
    
    function update_batch($table,$where,$data)
    {
		$this->db->protect_identifiers=true;
        $this->db->update_batch($table,$data,$where);
       // echo $this->db->last_query();
        return $this->db->affected_rows();
    }
    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        /*echo $this->db->last_query(); die();*/
        return $this->db->affected_rows();
    }
    
    function delete_batch($table,$key,$ids=array())
    {	
		$this->db->where_in($key,$ids);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
    
    function select($sel,$table,$cond = false,$cond_or = false, $join = array(), $cond_like = array(),$order = false, $field = false)
	{
		$this->db->protect_identifiers=true;
		$this->db->select($sel, FALSE);
		$this->db->from($table);
                //$this->db->join('tbl_privilege', ''.$table.'.privilage_id=tbl_privilege.privilege_id', 'left');
        if($cond)
        {        
    		foreach ($cond AS $k => $v)
    		{
    			if($v !=""){
    				$this->db->where($k,$v);
    			}
    			else{
    				$this->db->where($k);
    			}
    		}
        }
        if($cond_or)
        {
    		foreach ($cond_or AS $k => $v)
    		{
                //echo $v;
    			if($v !=""){
    				$this->db->or_where($k,$v);
    			}
    			else{
    				$this->db->where($k);
    			}
    		
    		}
        }
        foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,'left');
		}
		foreach ($cond_like AS $k => $v)
		{
			$this->db->like($k,$v,'after');
		}
		if ($order && $field) {
            $this->db->order_by($field, $order);
        }
		$query = $this->db->get();
       // echo $this->db->last_query();die;
		return $query->result_array();
	}
	
	function selectQuery($sel,$table,$cond = array(),$orderBy=array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function isUserExist($table,$where = array())
    {
		$this->db->select('user_id', FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    
    public function getMaster($tablename, $where = FALSE, $join = FALSE, $order = false, $field = false, $select = false,$limit=false,$start=false, $search=false)
    {
		if($limit){
			$this->db->limit($limit, $start);
		}
		if ($search) {
                $where = $this->searchString($search);
                $query = $this->db->get_where($tablename, $where);
        } 
        if ($where) {
            $this->db->where($where, NULL, FALSE);
        }
        if ($order && $field) {
            $this->db->order_by($field, $order);
        }
        if ($join) {
            if (count($join) > 0) {
                foreach ($join as $key => $value) {
                    $explode = explode('|', $value);
                    $this->db->join($key, $explode[0], $explode[1]);
                }
            }
        }
        if ($select) {
            $this->db->select($select, FALSE);
        } else {
            $this->db->select('*');
        }
		$query = $this->db->get($tablename);
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
    public function getRowsPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array())
    {
		$per_page=$this->session->userdata('perpage');
        $this->db->protect_identifiers=true;
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val,"LEFT");
        }
       
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
         
        $this->db->limit($per_page,($page*$per_page));
        $query = $this->db->get();
        //print_r($query );die;
        //echo $this->db->last_query();die;
        return $query->result_array();
    }

    public function getRowsFrontPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array(),$group_by="")
    {
    	if($this->session->userdata('perpage'))
    	{
    	$per_page=$this->session->userdata('perpage');
    	}else {
    	$per_page = 5;
    	}
    	$this->db->protect_identifiers=true;
    	$this->db->select($select, FALSE);
    	$this->db->from($table);
    	foreach ($cond AS $k => $v)
    	{
                           if($v !=""){
                                        $this->db->where($k,$v);
                            }
                            else{
                                    $this->db->where($k);
                            }
    	}
        
        
                    
    	$q = "";
    	foreach($like AS $k => $v)
    	{
    		$q .= $k." LIKE '%".$v."%' OR ";
    	}
    	if($q != ""){ $q = substr($q,0,-3);
    	$this->db->where("(".$q.")");
    	}
    	foreach($join as $key => $val)
    	{
    		$this->db->join($key, $val,"LEFT");
    	}
    	 
    	foreach($orderBy as $key => $val)
    	{
    		$this->db->order_by($key, $val);
    	}
    	if($group_by != ""){
    		$this->db->group_by($group_by);
    	}
    	$this->db->limit($per_page,($page*$per_page));
    	$query = $this->db->get();
    	//print_r($query );die;
    	//echo $this->db->last_query();die;
    	return $query->result_array();
    }
    
    
     public function getRowsViewsPerPage($select = "*", $table, $cond , $page = 0)
    {
         
    	if($this->session->userdata('perpage'))
    	{
    	$per_page=$this->session->userdata('perpage');
    	}else {
    	$per_page = 5;
    	}
    	$this->db->protect_identifiers=true;
    	$this->db->select($select, FALSE);
    	$this->db->from($table);
    	if($cond !="")
    	{
    		$this->db->where($cond);
    	}
    	
    	$this->db->limit($per_page,($page*$per_page));
    	$query = $this->db->get();
    	//print_r($query );die;
    	//echo $this->db->last_query();die;
    	return $query->result_array();
    }
    
   public function getCount($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(),$join=array())
    {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }
        if($join == TRUE){
	        foreach($join as $key => $val)
	        {
	            $this->db->join($key, $val,"LEFT");
	        }
        }
      
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
       
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    public function getCountIndivisualTable($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(),$join = array())
    {
    	$this->db->select($select, FALSE);
    	$this->db->from($table);
                    foreach($cond AS $k => $v){
                        if($v !=""){
                                $this->db->where($k,$v);
                                    }
                                    else{
                                            $this->db->where($k);
                                    }
                    }
    	$q = "";
    	foreach($like AS $k => $v)
    	{
    		$q .= $k." LIKE '%".$v."%' OR ";
    	}
    	if($q != ""){ $q = substr($q,0,-3);
    	$this->db->where("(".$q.")");
    	} 
    	foreach($orderBy as $key => $val)
    	{
    		$this->db->order_by($key, $val);
    	}
        if(!empty($join)){
            foreach($join as $key => $val)
            {
                    $this->db->join($key, $val,"left");
            } 
        }
      
    	$query = $this->db->get();
    	return $query->result_array();
    }
    
    
    
    public function getCountViewTable($select = "*", $table, $cond)
    {
    	$this->db->select($select, FALSE);
    	$this->db->from($table);
    	if($cond !="")
    	{
    		$this->db->where($cond);
    	}
    	
    	$query = $this->db->get();
    	return $query->result_array();
    }
    
   public function get_selected_data($table,$select,$field_in,$array)
   {
    
    //$query = "SELECT * FROM `$table` WHERE `$column` IN(".implode(',',$array).")";
    $this->db->select($select, FALSE);
    $this->db->from($table);
    $this->db->where($field_in.' in('.$array.')');
    $query = $this->db->get();
   // echo $this->db->last_query();
    return $query->result_array();
   }
    public function get_selected_data_rss($table,$select,$field_in,$array)
   {
    
    //$query = "SELECT * FROM `$table` WHERE `$column` IN(".implode(',',$array).")";
    $this->db->select($select, FALSE);
    $this->db->from($table);
   //  $this->db->join('credentials', '$table.functionalArea_fk_id = tbl_functionalArea.functionalArea_id', 'left'); 
    $this->db->where($field_in.' in('.$array.')');
    $query = $this->db->get();
   // echo $this->db->last_query();
    return $query->result_array();
   }
   
   public function searchString($search)
   {
   
   	if($search!=""){
   		foreach ($arrayColumn as $key => $value) {
   			if(array_key_exists($key,$arrayStatus)){
   				foreach ($arrayStatus[$key] as $status => $s) {
   					if(strtolower($search) == strtolower($status)){
   						$arrayLike[$value] = $s;
   						break;
   					}
   				}
   			}
   			else
   			{
   				return $arrayLike[$value] = $search;
   			}
   		}
   	}
   	else
   	{
   		return $arrayLike = array();
   	}
   
   }

   public function getCountData($select = "*", $table, $cond = array(),$group_by="")
    {        
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }        
        if($group_by != ""){
            $this->db->group_by($group_by);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        return $query->result_array();
    }
    
 
   
    public function getData($tablename, $where = FALSE, $join = FALSE, $order = false, $field = false, $select = false,$limit=false,$start=false, $search=false,$group_by=false)
    {
		if($limit){
			$this->db->limit($limit, $start);
		}
		if ($search) {
                $where = $this->searchString($search);
                $query = $this->db->get_where($tablename, $where);
        } 
        if ($where) {
            $this->db->where($where, NULL, FALSE);
        }
        if($group_by)
        {
            $this->db->group_by($group_by);
        }
        if ($order && $field) {
            $this->db->order_by($field, $order);
        }
        if ($join) {
            if (count($join) > 0) {
                foreach ($join as $key => $value) {
                    $explode = explode('|', $value);
                    $this->db->join($key, $explode[0], $explode[1]);
                }
            }
        }
        if ($select) {
            $this->db->select($select, FALSE);
        } else {
            $this->db->select('*');
        }
		$query = $this->db->get($tablename);
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
    public function getData_1($tablename, $where = FALSE, $join = FALSE, $order = false, $field = false, $select = false,$limit=false,$start=false, $search=false,$group_by=false)
    {
        if($limit){
            $this->db->limit($limit, $start);
        }
        if ($search) {
                $where = $this->searchString($search);
                $query = $this->db->get_where($tablename, $where);
        } 
        if ($where) {
            $this->db->where($where, NULL, FALSE);
        }
        if($group_by)
        {
            $this->db->group_by($group_by);
        }
        if ($order && $field) {
            $this->db->order_by($field, $order);
        }
        if ($join) {
            if (count($join) > 0) {
                foreach ($join as $key => $value) {
                    $explode = explode('|', $value);
                    $this->db->join($key, $explode[0], $explode[1]. 'left');
                }
            }
        }
        if ($select) {
            $this->db->select($select, FALSE);
        } else {
            $this->db->select('*');
        }
        $query = $this->db->get($tablename);
        //echo $this->db->last_query();die;
        return $query->result_array();
    }

    function get_chart_data_for_visits($select, $table, $cond) {
        $sql = 'SELECT '.$select.' FROM ' . $table . ' WHERE '.$cond.' ORDER BY employment_id ASC';
        //$sql = 'SELECT '.$select.' FROM ' . $table . ' WHERE '.$cond;
        $query = $this->db->query($sql);
        $result = $query->result_array();

        foreach ($result as $key => $value) {
            $exp = json_decode($value['employment_total_experience'],true);
            $sal = json_decode($value['employment_annualSalary'], true);
            $dataArray[$key]['label'] = $exp['year'].'.'.$exp['month'];
            $dataArray[$key]['value'] = $sal['lakh'].'.'.$sal['thousand'];
        }
        
        return $dataArray;
    }
}
?>

