<?php 

class Class_master_model extends CI_Model{ 

	function __construct()
	{
		parent::__construct();
		$this->load->database();        
		$this->load->model('common_model');        
	}

	public function upload_class($class_data)
	{
		$this->common_model->insert('class_master_ut', $class_data);
	}