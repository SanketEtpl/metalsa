<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("common_model");        
        $this->load->library('excel');
        header('Access-Control-Allow-Origin: *');
        ini_set('display_errors', '0');        
    }

    // ---- save class master by uploading excel file -----
    public function upload_class_master_post()    
    {
        
        $postData = $_POST;
        $this->load->model("class_master_model");
        $configUpload['upload_path'] = FCPATH.'uploads/excel/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';
        $configUpload['max_size'] = '5000';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('userfile');  
         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
         $file_name = $upload_data['file_name']; //uploded file name
         $extension=$upload_data['file_ext'];    // uploded file extension

//$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
 $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007     
          //Set to read only
 $objReader->setReadDataOnly(true);          
        //Load excel file
 $objPHPExcel=$objReader->load(FCPATH.'uploads/excel/'.$file_name);      
         $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
         $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
          //loop from first data untill last data
         for($i=2;$i<=$totalrows;$i++)
         {
            $className= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();           
            $classDesc= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
            $major_minor_flag= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
            $data_user=array(
                'class_name'=>$className,
                'class_desc'=>$classDesc,
                'major_minor_flag'=>$major_minor_flag,
                'status'=>'1',
                'created_date'=>date('Y-m-d H:i:s'),
                'user_id'=>'1'
            );
            $this->class_master_model->upload_class($data_user);


        }
             unlink('././uploads/excel/'.$file_name); //File Deleted After uploading in database .           
             // redirect(base_url() . "put link were you want to redirect");

         }


     }
